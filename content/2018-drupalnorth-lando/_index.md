---
title: "Administer your personal Cloud City with Lando"
outputs: ["Reveal"]
---
{{% section %}}
{{% slide id="hometitle" background-image="/images/cloud-city-millenium-falcon3.png" %}}

## LANDO
### Administer Your Own
### Cloud City
#### Derek Laventure
##### derek@consensus.enterprises
<div class="whitebg"><img src="/images/consensus-logo-v1.1.svg"></div>

<small> ([DrupalNorth 2018](http://drupalnorth.org/en/session/lando-administer-your-own-cloud-city))</small>
{{% /slide %}}
{{% /section %}}
