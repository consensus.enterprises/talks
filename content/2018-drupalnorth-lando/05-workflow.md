---
title: "Workflow"
outputs: ["Reveal"]
weight: 50
---
{{% section %}}
{{% slide id="workflow" background-image="/images/cloud-city-easter-egg.jpg" %}}

# Workflow

{{% /slide %}}
{{% slide id="workflow-drumkit" background-image="/images/cloud-city-easter-egg.jpg" %}}

## Drumkit

![Drumkit is a suite of Makefiles to simplify Drupal development](/images/drumkit-github-readme.png)

{{% /slide %}}
{{% slide id="workflow-starwars1" background-image="" %}}

![You've got a lot of nerve](/images/youvegotalotofnerve.gif)

{{% /slide %}}
{{% slide id="workflow-drumkit2" background-image="/images/cloud-city-easter-egg.jpg" %}}

## Drumkit Setup

```
wget -O - https://raw.githubusercontent.com/ergonlogic/drumkit/master/scripts/install.sh | /bin/bash
```

```
git submodule add https://github.com/ergonlogic/drumkit .mk
echo "include .mk/GNUMakefile" >> Makefile
ln -s .mk/d
source d
```

{{% /slide %}}
{{% slide id="workflow-drumkit3" background-image="/images/cloud-city-easter-egg.jpg" %}}

## Drumkit Help

![](/images/make-help.png)

{{% /slide %}}
{{% slide id="workflow-starwars2" background-image="" %}}

![Lando Calrissian!](/images/strangerthings-lando.gif)

{{% /slide %}}
{{% slide id="workflow-gitlab" background-image="/images/cloud-city-easter-egg.jpg" %}}

## .gitlab-ci.yml

```
build:make:install:
  stage: build
  before_script:
    # Bootstrap Drumkit
    - . d
  script:
    - make build VERBOSE=1
    # Check that scaffolding has been built.
    - "[ -f web/index.php ]"

test:behat:default: &test
  stage: test
  services:
    - name: mysql:5.7
      alias: cop_db
  cache:
    <<: *cache
    policy: pull
  variables:
    PROFILE: default
  before_script:
    # Bootstrap Drumkit
    - . d
    # Build the codebase.    @TODO cache this from the build/install phase.
    - make build VERBOSE=1
    # Install the site.      @TODO cache a backup from the build/install phase, and restore it here.
    - make install VERBOSE=1
    # Configure and start Apache, and related tasks.
    - ./scripts/apache/setup.sh
    # Check that sites have been installed.
    - "[ -d web/sites/cop.lndo.site ]"
    - drush --uri=cop.lndo.site status
  script:
    - make ci
```

{{% /slide %}}
{{% slide id="workflow-starwars3" background-image="" %}}

![Woohoo](/images/millenium-falcon-wohoo.gif)

{{% /slide %}}
{{% slide id="workflow-xdebug" background-image="/images/cloud-city-easter-egg.jpg" %}}

## Debugging

* [PHPStorm && XDebug](https://docs.devwithlando.io/services/php.html#using-xdebug)
* [Lando tooling](https://docs.devwithlando.io/tutorials/setup-additional-tooling.html)


```
  dynamic:
    # Just print the hostname so we know what service we are on
    cmd: printenv LANDO_SERVICE_NAME

    # The format :NAME will use the service from NAME flag
    # You likely will want to specify the option below 
    # and set a reasonable default
    service: :service

    # Specify a service option to handle the above
    options:
      service:
        default: appserver
        alias:
          - s
        describe: Run a different service
```

{{% /slide %}}
{{% slide id="workflow-starwars4" background-image="" %}}

![Lando hugs](/images/lando-hugs.gif)

{{% /slide %}}
{{% /section %}}
