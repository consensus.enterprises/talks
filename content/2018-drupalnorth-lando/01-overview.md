---
title: "Overview"
outputs: ["Reveal"]
weight: 10
---
{{% section %}}
{{% slide id="overview" background-image="/images/Billy-Dee-Williams-as-Lando-Calrissian-darkoverlay.jpg" %}}

## Overview

<ol>
{{% fragment %}}<li>Assumptions</li>{{% /fragment %}}
{{% fragment %}}<li>Basics</li>{{% /fragment %}}
{{% fragment %}}<li>Customizing</li>{{% /fragment %}}
{{% fragment %}}<li>Workflow</li>{{% /fragment %}}
{{% fragment %}}<li>PLEASE ASK QUESTIONS!</li>{{% /fragment %}}
</ol>

{{% /slide %}}
{{% slide id="overview-assumptions" background-image="/images/Billy-Dee-Williams-as-Lando-Calrissian-darkoverlay.jpg" %}}

## Assumptions

You have some familiarity with:
<ul>
{{% fragment %}}<li>Docker & docker-compose</li>{{% /fragment %}}
{{% fragment %}}<li>Git</li>{{% /fragment %}}
{{% fragment %}}<li>Drupal dev workflow (composer etc)</li>{{% /fragment %}}
{{% fragment %}}<li>Recommended: install Lando from source</li>{{% /fragment %}}
</ul>
{{% fragment %}}<pre>https://docs.devwithlando.io/installation/installing.html#from-source</pre>{{% /fragment %}}

{{% /slide %}}
{{% /section %}}
