---
title: "Customizing"
outputs: ["Reveal"]
weight: 40
---
{{% section %}}
{{% slide id="customizing" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

# Customizing

{{% /slide %}}

{{% slide id="customizing-switch" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

## s/apache/nginx/

```
--- a/.lando.yml
+++ b/.lando.yml
@@ -10,5 +10,5 @@ proxy:
 services:
   appserver:
     type: php:7.2
-    via: apache
+    via: nginx
     ssl: true
```

{{% /slide %}}
{{% slide id="customizing-starwars1" background-image="" %}}

![What have we here?](/images/whathavewehere.gif)

{{% /slide %}}
{{% slide id="customizing-add" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

## [Adding Services](https://docs.devwithlando.io/config/services.html)

### eg. MailHog...

```
services:
  mailhog:
    type: mailhog
    hogfrom:
      - appserver

proxy:
  mailhog:
    - mh.chewie.lndo.site
```

{{% /slide %}}
{{% slide id="customizing-add2" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

### ...or whatever

<ul>
{{% fragment %}}<li>Elasticsearch</li>{{% /fragment %}}
{{% fragment %}}<li>MongoDB</li>{{% /fragment %}}
{{% fragment %}}<li>NodeJS</li>{{% /fragment %}}
{{% fragment %}}<li>[etc..](https://docs.devwithlando.io/config/services.html)</li>{{% /fragment %}}
</ul>


{{% /slide %}}
{{% slide id="customizing-conf" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

## Custom config

* custom php.ini & database credentials
 
```
services:
  appserver:
    config:
      conf: php.ini
  database:
    creds:
      user: l337
      password: droidlife
      database: falcon
```

{{% /slide %}}
{{% slide id="customizing-starwars2" background-image="" %}}

![Lando looks at Boba Fett](/images/lando-bobafett.gif)

{{% /slide %}}
{{% slide id="customizing-config" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

## The Kessel Run

<ul>
{{% fragment %}}<li>install 2 drupal sites!</li>{{% /fragment %}}
{{% fragment %}}<li>example: SSO setup</li>{{% /fragment %}}
</ul>

{{% /slide %}}
{{% slide id="customizing-sso" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

## .lando.yml

```
name: ra
compose:
  - chromedriver-compose.yml
recipe: drupal8
config:
  php: '7.1'
  webroot: web
  drupal: true
  xdebug: true
```

{{% /slide %}}
{{% slide id="customizing-sso2" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

### .lando.yml services

```
services:
  # RA site database.
  ra_db:
    type: mysql:5.7
    creds:
      user: sso-ra
      password: sso-ra
      database: sso-ra
    portforward: 32777

  # RA site web server
  ra_web:
    type: php:7.1
    via: apache
    webroot: web
    ssl: true
    config:
      server: config/apache/httpd-ssl-ra.conf
    xdebug: true
    overrides:
      services:
        volumes:
          - downloads:/downloads
```

{{% /slide %}}
{{% slide id="customizing-sso3" background-image="/images/Lando-Calrissian-Cloud-City-Administrator.jpg" %}}

```
  # RP site database.
  rp_db:
    type: mysql:5.7
    creds:
      user: sso-rp
      password: sso-rp
      database: sso-rp
    portforward: 32778

  # RP site web server.
  rp_web:
    type: php:7.1
    via: apache
    webroot: web
    ssl: true
    config:
      server: config/apache/httpd-ssl-rp.conf
    xdebug: true

  mailhog:
    type: mailhog
    hogfrom:
      - ra_web
      - rp_web
```

{{% /slide %}}
{{% slide id="customizing-starwars3" background-image="" %}}
![Unquestionably Awesome](/images/unquestionably-awesome.gif)
{{% /slide %}}
{{% /section %}}
