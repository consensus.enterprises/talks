---
title: "Why not.."
outputs: ["Reveal"]
weight: 20
---
{{% section %}}
{{% slide id="whynot" background-image="/images/Lando-Featured-041718.jpg" %}}

# Why not...

<ul>
{{% fragment %}}<li>DrupalVM?</li>{{% /fragment %}}
{{% fragment %}}<li>DDev?</li>{{% /fragment %}}
{{% fragment %}}<li>Docksal?</li>{{% /fragment %}}
{{% fragment %}}<li>[your favourite here]</li>{{% /fragment %}}
</ul>
{{% /slide %}}
{{% slide id="whynot-starwars1" background-image="" %}}

![Donald Glover shooting](/images/glover-shooting.gif)

{{% /slide %}}
{{% slide id="whynot-docker" background-image="/images/Lando-Featured-041718.jpg" %}}

# "vanilla" Docker Compose

{{% fragment %}}**Moshe:** ```http://medium.com/ma-digital-services/dev-env-5d35b97f3473```{{% /fragment %}}

{{% /slide %}}
{{% slide id="whynot-starwars2" background-image="" %}}

![Good Luck!](/images/goodluck.gif)

{{% /slide %}}
{{% slide id="whynot-lagoon" background-image="/images/Lando-Featured-041718.jpg" %}}

# Shout Out

![lagoon.readthedocs.io](/images/lagoon.png)

{{% fragment %}}```https://lagoon.readthedocs.io```{{% /fragment %}}

{{% /slide %}}
{{% slide id="whynot-starwars3" background-image="" %}}

![Who me?](/images/han-who-me.gif)

{{% /slide %}}
{{% /section %}}
