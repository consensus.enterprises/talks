---
title: "Basics"
outputs: ["Reveal"]
weight: 30
---
{{% section id="basic" %}}
{{% slide id="basics" background-image="/images/cloud-city-millenium-falcon.jpg" %}}

# Lando Basics

{{% /slide %}}

{{% slide id="basics-cleanslate" background-image="/images/cloud-city-millenium-falcon.jpg" %}}

## D8 From Scratch

```
# Basic Drupal setup
mkdir chewie
composer create-project drupal-composer/drupal-project:8.x-dev \
   chewie --stability dev --no-interaction

# Git setup
cd chewie 
git init && git add .
git commit -m "Initial commit: chewie.lndo.site"

# Lando!
lando init --name=chewie --recipe=drupal8 --webroot=web
git add .lando.yml && git commit -m "lando init"
lando start
```

{{% fragment %}} `http://chewie.lndo.site` {{% /fragment %}}

{{% /slide %}}
{{% slide id="basics-cleanyaml" background-image="/images/cloud-city-millenium-falcon.jpg" %}}

## .lando.yml

```
name: chewie
recipe: drupal8
config:
  webroot: web
```

{{% /slide %}}
{{% slide id="basics-clean-composer" background-image="/images/cloud-city-millenium-falcon.jpg" %}}

## ~/.lando/compose/
## chewie/chewie-1.yml

```
$ wc -l ~/.lando/compose/chewie/chewie-1.yml 
127 /home/spiderman/.lando/compose/chewie/chewie-1.yml
```

{{% /slide %}}
{{% slide id="basics-starwars1" background-image="" %}}

![Donald Glover Oooooh](/images/donaldglover-whistling.gif)

{{% /slide %}}
{{% slide id="basics-landofy" background-image="/images/cloud-city-millenium-falcon.jpg" %}}

## Lando-fy

```
lando init --recipe=drupal7 --name=smithwriter --webroot=.
lando start
lando db-import database.sql
```

### .lando.yml

```
name: smithwriter
recipe: drupal7
config:
  webroot: web
```

{{% /slide %}}
{{% slide id="basics-starwars2" background-image="" %}}

![Buckle Up](/images/glover-buckleup.gif)

{{% /slide %}}

{{% slide id="basics-grav" background-image="/images/cloud-city-millenium-falcon.jpg" %}}


## Other

```
unzip grav-skeleton-onepage-site-v2.0.0.zip
mv grav-skeleton-onepage-site grav

cd grav
lando init --recipe=custom --name=grav --webroot=.
```

{{% /slide %}}
{{% slide id="basics-grav2" background-image="/images/cloud-city-millenium-falcon.jpg" %}}


## GravCMS .lando.yml

```
name: grav
recipe: custom
config:
  webroot: .

proxy:
  appserver:
    - grav.lndo.site

services:
  appserver:
    type: php:7.2
    via: apache
    ssl: true
```

```
lando start
```

{{% /slide %}}
{{% slide id="basics-starwars3" background-image="" %}}

![Yeeehaaa!](/images/yeeehaaaa.gif)

{{% /slide %}}
{{% slide id="basics-recipes" background-image="/images/cloud-city-millenium-falcon.jpg" %}}

## Recipes

##### plugins/lando-recipes/recipes/drupal7/drupal7.js

```
  /*
   * Build out Drupal7
   */
  var build = function(name, config) {

    // Get the via so we can grab our builder
    var base = (_.get(config, 'via', 'apache') === 'apache') ? 'lamp' : 'lemp';

    // Update with new config defaults if needed
    config = helpers.resetConfig(config._recipe, config);

    // Set the default php version for D7
    config.php = _.get(config, 'php', '7.0');

    // Start by cheating
    var build = lando.recipes.build(name, base, config);

    // Determine the default drush setup for D7
    var defaultDrush = (config.php === '5.3' ? DRUSH7 : DRUSH8);

    // Get the drush config
    var drushConfig = _.get(config, 'drush', 'global:' + defaultDrush);

    // Handle drush
    var buildersKey = 'services.appserver.run_internal';
    build.services.appserver.run_internal = _.get(build, buildersKey, []);
    build = drush(build, drushConfig);

    // Return the things
    return build;

  };

  // Return the things
  return {
    build: build,
    configDir: __dirname
  };
```

{{% /slide %}}
{{% /section %}}
