---
title: "Closing"
outputs: ["Reveal"]
weight: 60
---
{{% section %}}
{{% slide id="links" background-image="/images/lando-chewie.jpeg" %}}

## Background Links

* [drupal.org: Docker Development Environments](https://www.drupal.org/docs/develop/local-server-setup/docker-development-environments)
* [drupalize.me: Drupal Development with Docker](https://drupalize.me/series/drupal-development-docker)

* [Using Docker for local Drupal development](https://codekoalas.com/blog/using-docker-local-drupal-development)
* [Local Drupal Development Roundup](https://www.lullabot.com/articles/local-drupal-development-roundup)

* [Our modern development environment at Mass.gov](https://medium.com/ma-digital-services/dev-env-5d35b97f3473)
* [DrupalVM experimental Docker support](http://docs.drupalvm.com/en/latest/other/docker/)

{{% /slide %}}
{{% slide id="closing" background-image="/images/lando-chewie.jpeg" %}}

# [Thanks!](#hometitle)

{{% /slide %}}
{{% /section %}}
