---
title: Consensus Enterprises Talks and Presentations
outputs: ["Reveal"]
---
{{% section %}}
{{% slide id="home" background-image="/images/blueskies.jpg" %}}

#### Talks & Presentations

* 2022-09 @ [Drupal Diveristy Camp](https://www.drupaldiversity.com/initiatives/ddi-camp-2022/program): **[Retiring the 40hr work week](2022-ddi-camp-retiring-40hr-workweek)** ([Derek Laventure](https://consensus.enterprises/team/dereklaventure))
* 2021-11 @ [DrupalTO](https://www.meetup.com/DrupalTO/events/281866819/): **[Demo of D7->D9 progressive upgrade](2021-d7d9-progressive-upgrade)** ([Christopher Gervais](https://consensus.enterprises/team/christopher))
* 2020-11 @ [DrupalTO](https://www.meetup.com/DrupalTO/events/274287188/): **[Goldilocks & the 3 Projects: Behat, BDD & Getting specs juuust Right](2020-goldilocks-bdd)** ([Dan Friedman](https://consensus.enterprises/team/dan/))
* 2020-10 @ [DrupalGovCon](https://www.drupalgovcon.org/2020/program/sessions/automate-all-things): **[Automate all the things](2020-drupalgovcon-automate-all-the-things)** ([Dan Friedman](https://consensus.enterprises/team/dan/), [Colan Schwartz](https://consensus.enterprises/team/colan) and [Christopher Gervais](https://consensus.enterprises/team/christopher))
* 2019-06 @ [Drupal North](https://drupalnorth.org/): **[Drupal SaaS: Building software as a service on Drupal](2019-drupalnorth-drupal-saas)** ([Colan Schwartz](https://consensus.enterprises/team/colan/))
* 2018-10 @ [DrupalCamp Ottawa](https://2018.drupalcampottawa.com/): **[Drupal SaaS: Building software as a service on Drupal](2018-drupalcamp-ottawa-drupal-saas)** ([Colan Schwartz](https://consensus.enterprises/team/colan/))
* 2018-08 @ [Drupal North](https://2018.drupalnorth.org/): **[Lando: Administer your own Cloud City](2018-drupalnorth-lando)** ([Derek Laventure](https://consensus.enterprises/team/derek/))

<div class="whitebg bottom-right"><img src="/images/consensus-logo-v1.1.svg"></div>
{{% /slide %}}
{{% /section %}}
