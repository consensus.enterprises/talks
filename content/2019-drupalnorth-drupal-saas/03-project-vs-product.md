---
title: "Project vs. Product"
outputs: ["Reveal"]
weight: 30
---
{{% section id="project-vs-product" %}}
{{% slide id="project-vs-product" %}}

## Project vs. Product

<ul>
{{% fragment %}}<li>Different than development contracts</li>{{% /fragment %}}
{{% fragment %}}<li>Lots of unpaid work to build platform</li>{{% /fragment %}}
{{% fragment %}}<li>Have a plan for keeping the lights on</li>{{% /fragment %}}
{{% fragment %}}<li>Consulting?  Investors?  Rich relative?</li>{{% /fragment %}}
{{% fragment %}}<li>Now: Lots of development.  No regular income.</li>{{% /fragment %}}
{{% fragment %}}<li>Later: Little development.  Lots of regular income.</li>{{% /fragment %}}
</ul>

{{% fragment %}}...but only if you're successful.{{% /fragment %}}

{{% /slide %}}
{{% /section %}}
