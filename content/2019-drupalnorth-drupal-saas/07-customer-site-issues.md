---
title: "Customer Site Issues"
outputs: ["Reveal"]
weight: 70
---
{{% section id="customer-site-issues" %}}
{{% slide id="customer-site-overview" %}}

## Customer site issues

<ul>
  {{% fragment %}}<li>[Installation profiles](https://www.drupal.org/docs/8/creating-distributions/how-to-write-a-drupal-8-installation-profile) (distros) vs. [Features](https://www.drupal.org/project/features)</li>{{% /fragment %}}
  {{% fragment %}}<li>Resource quotas<ul>
   {{% fragment %}}<li>[Site Quota Enforcer](https://www.drupal.org/project/quenforcer)</li>{{% /fragment %}}
  </ul></li>{{% /fragment %}}
  {{% fragment %}}<li>Site admin access<ul>
   {{% fragment %}}<li>User 1 vs. Owner role</li>{{% /fragment %}}
   {{% fragment %}}<li>[Permissions Subset](https://www.drupal.org/project/subpermissions) (needs D8 port)</li>{{% /fragment %}}
   </ul></li>{{% /fragment %}}
</ul>

{{% /slide %}}

{{% /section %}}
