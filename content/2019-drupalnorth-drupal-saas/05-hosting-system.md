---
title: "Aegir Hosting System"
outputs: ["Reveal"]
weight: 50
---
{{% section id="hosting" %}}

{{% slide id="need-hosting-system" %}}
## But you still need a hosting system!
{{% /slide %}}

{{% slide id="write-hosting-system" %}}
## Do you need to write one from scratch?
{{% fragment %}}No!{{% /fragment %}}
{{% /slide %}}

{{% slide id="aegir-hosting-system" %}}
### Why?
{{% fragment %}}Aegir!{{% /fragment %}}
{{% /slide %}}

{{% slide id="what-is-aegir" %}}
## Aegir Hosting System
<ul>
{{% fragment %}}<li>Designed for hosting Drupal sites</li>{{% /fragment %}}
{{% fragment %}}<li>Open source</li>{{% /fragment %}}
{{% fragment %}}<li>10+ years!</li>{{% /fragment %}}
{{% fragment %}}<li>Web services API</li>{{% /fragment %}}
</ul>
<br />
{{% fragment %}}*...and will soon host anything*{{% /fragment %}}
{{% /slide %}}

{{% slide id="heard-of-aegir" %}}
### How many of you...
<ul>
{{% fragment %}}<li>Have heard of Aegir?</li>{{% /fragment %}}
{{% fragment %}}<li>Are using Aegir?</li>{{% /fragment %}}
{{% fragment %}}<li>Were using Aegir?</li>{{% /fragment %}}
</ul>
{{% /slide %}}

{{% /section %}}
