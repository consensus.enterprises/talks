---
title: "Interested in Drupal SaaS?"
outputs: ["Reveal"]
weight: 20
---
{{% section %}}
{{% slide id="ground-rules" %}}

## Ground Rules

<ul>
{{% fragment %}}<li>Please interrupt at any time!
  <ul><li>Ask questions & share knowledge</li></ul></li>{{% /fragment %}}
{{% fragment %}}<li>Feel free to follow along in your own browser
  <ul><li><a href="http://talks.consensus.enterprises/2019-drupalnorth-drupal-saas">talks.consensus.enterprises/2019-drupalnorth-drupal-saas</a></li></ul></li>{{% /fragment %}}
{{% fragment %}}<li>Presentation is licensed for reuse</li>
  <ul><li><a href="https://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International</a></li></ul></li>{{% /fragment %}}
</ul>

{{% /slide %}}
{{% slide id="interested-in-drupal-saas" %}}

## Interested in Drupal SaaS?

How many of you are...

<ul>
{{% fragment %}}<li>considering building a SaaS product?</li>{{% /fragment %}}
{{% fragment %}}<li>currently working on a SaaS product?</li>{{% /fragment %}}
{{% fragment %}}<li>already selling a SaaS product?</li>{{% /fragment %}}
{{% fragment %}}<li>just realizing you're in the wrong room?</li>{{% /fragment %}}
</ul>

{{% fragment %}}Other interests?{{% /fragment %}}

{{% /slide %}}

{{% /section %}}
