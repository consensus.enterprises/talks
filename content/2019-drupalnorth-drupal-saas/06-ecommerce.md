---
title: "E-commerce, recurring billing and subscription services"
outputs: ["Reveal"]
weight: 60
---
{{% section id="ecommerce" %}}
{{% slide id="ecommerce-integration" %}}

### So you now have a hosting system...

{{% fragment %}}How do you integrate it with:{{% /fragment %}}

<ul>
{{% fragment %}}<li>e-commerce</li>{{% /fragment %}}
{{% fragment %}}<li>recurring billing</li>{{% /fragment %}}
{{% fragment %}}<li>subscription services</li>{{% /fragment %}}
</ul>

{{% /slide %}}

{{% slide id="aegir-site-subscriptions" %}}
## [Aegir Site Subscriptions](https://www.drupal.org/project/aegir_site_subscriptions)
<blockquote>
Adds e-commerce to the Aegir ecosystem by associating hosted sites with customer subscriptions via recurring billing. Communicates with the Aegir API over Web services.
</blockquote>
{{% /slide %}}

{{% slide id="aegir-site-subscriptions-process" %}}
## Aegir Site Subscriptions
### Process
<ol>
{{% fragment %}}<li>Customer selects a plan</li>{{% /fragment %}}
{{% fragment %}}<li>Subscription service takes payment info</li>{{% /fragment %}}
{{% fragment %}}<li>Customer's site gets provisioned</li>{{% /fragment %}}
{{% fragment %}}<li>Site gets deleted on payment failures</li>{{% /fragment %}}
</ol>
{{% /slide %}}

{{% slide id="subscription-providers" %}}
## Aegir Site Subscriptions
### Subscription Providers
Plug-ins
<ol>
{{% fragment %}}<li>[Recurly](https://www.drupal.org/project/recurly) (done)</li>{{% /fragment %}}
{{% fragment %}}<li>[Commerce Recurring Framework](https://www.drupal.org/project/commerce_recurring) (ideal, needs funding)</li>{{% /fragment %}}
{{% fragment %}}<li>Others (need funding)</li>{{% /fragment %}}
</ol>
{{% /slide %}}

{{% /section %}}
