---
title: "Hosting"
outputs: ["Reveal"]
weight: 40
---
{{% section id="infrastructure" %}}
{{% slide id="drupal-hosting-companies" %}}

# Hosting

{{% /slide %}}

{{% slide id="drupal-hosting-companies" %}}

### Drupal Hosting Companies

{{% fragment %}}Pros{{% /fragment %}}

<ul>
{{% fragment %}}<li>Outsource infrastructure</li>{{% /fragment %}}
{{% fragment %}}<li>Simplify site maintenance</li>{{% /fragment %}}
</ul>

{{% fragment %}}Cons{{% /fragment %}}

<ul>
{{% fragment %}}<li>Vendor lock-in</li>{{% /fragment %}}
{{% fragment %}}<li>No control over hosting costs</li>{{% /fragment %}}
{{% fragment %}}<li>No control over data centre locations</li>{{% /fragment %}}
{{% fragment %}}<li>Non-portable configuration as code</li>{{% /fragment %}}
{{% fragment %}}<li>Don't support multisite</li>{{% /fragment %}}
{{% fragment %}}<li>Costs may not scale well for many sites</li>{{% /fragment %}}
</ul>

{{% /slide %}}

{{% slide id="proprietary-iaas" %}}

### Infrastructure

{{% fragment %}}Proprietary IaaS{{% /fragment %}}

<ul>
{{% fragment %}}
  <li>Amazon Web Services (AWS)</li>
  <li>Google Cloud Services (GCS)</li>
  <li>Microsoft Azure</li>
{{% /fragment %}}
</ul>

{{% fragment %}}Open-source IaaS{{% /fragment %}}

<ul>
{{% fragment %}}<li>[OpenStack](https://en.wikipedia.org/wiki/OpenStack)</li>{{% /fragment %}}
</ul>

{{% /slide %}}

{{% slide id="open-source-iaas" %}}

### Infrastructure

OpenStack

<ul>
{{% fragment %}}<li>No vendor lock-in</li>{{% /fragment %}}
{{% fragment %}}<li>Often no data in & out charges</li>{{% /fragment %}}
{{% fragment %}}<li>Portable configuration as code: standard API</li>{{% /fragment %}}
{{% fragment %}}<li>Data centres in various countries</li>{{% /fragment %}}
{{% fragment %}}<li>Data portability (e.g. export & import VMs)</li>{{% /fragment %}}
{{% fragment %}}<li>More control over hosting costs</li>{{% /fragment %}}
</ul>

{{% /slide %}}
{{% /section %}}
