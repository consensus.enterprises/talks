---
title: "The End"
outputs: ["Reveal"]
weight: 90
---
{{% section id="the-end" %}}

{{% slide id="thank-you" %}}
# Thank you!
{{% /slide %}}

{{% slide id="feedback" %}}
## Feedback
<ul>
  <li>Questions?</li>
  <li>Comments?</li>
  <li>Thoughts?</li>
</ul>
{{% /slide %}}

{{% /section %}}
