---
title: "Who am I?"
outputs: ["Reveal"]
weight: 10
---
{{% section %}}
{{% slide id="whoami" %}}

## Who am I?

<ul>
{{% fragment %}}<li>On Drupal.org for 12 years 5 months as <a href="https://www.drupal.org/u/colan">colan</a></li>{{% /fragment %}}
{{% fragment %}}<li>Maintain <a href="https://www.drupal.org/colan-schwartz-consulting#projects-supported">over 40 modules</a></li>{{% /fragment %}}
{{% fragment %}}<li>Worked on first release of <a href="https://buyandsell.gc.ca/">buyandsell.gc.ca</a></li>{{% /fragment %}}
{{% fragment %}}<li>[GSOC](https://en.wikipedia.org/wiki/Google_Summer_of_Code) Mentor for data-encryption projects</li>{{% /fragment %}}
{{% fragment %}}<li>Core Maintainer of the <a href="https://www.aegirproject.org/">Aegir Hosting System</a></li>{{% /fragment %}}
{{% fragment %}}<li>Enterprise Cloud Architect</li>{{% /fragment %}}
{{% fragment %}}<li>Drupal/Aegir/SaaS/PaaS/SaaP/IaaS Consultant</li>{{% /fragment %}}
{{% fragment %}}<li>Founding Partner of <a href="https://consensus.enterprises/">Consensus Enterprises</a></li>{{% /fragment %}}
</ul>

{{% /slide %}}
{{% slide id="consensus" %}}

## Consensus Enterprises

*Helping you do big things in the cloud*

<ul>
{{% fragment %}}<li>DevOps processes & documentation</li>{{% /fragment %}}
{{% fragment %}}<li>Self-hosted multisite solutions & audits</li>{{% /fragment %}}
{{% fragment %}}<li>Application lifecycle management</li>{{% /fragment %}}
{{% fragment %}}<li>Continuous integration/delivery (CI/CD)</li>{{% /fragment %}}
{{% fragment %}}<li>Cloud infrastructure</li>{{% /fragment %}}
{{% fragment %}}<li>Software-as-a-service engineering</li>{{% /fragment %}}
</ul>

{{% /slide %}}
{{% /section %}}
