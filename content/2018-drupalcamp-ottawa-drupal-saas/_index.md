---
title: "Drupal SaaS: Building software as a service on Drupal"
outputs: ["Reveal"]
---
{{% section %}}
{{% slide id="drupal-saas" background-image="/images/drupal-saas/DCO-2018--16-9--Presentation-Slide.png" %}}

## Drupal SaaS

<br />

#### [Building software as a service on Drupal](http://talks.consensus.enterprises/drupal-saas)


##### [Colan Schwartz](https://consensus.enterprises/team/colan/)

<div class="whitebg">
  <a href="https://consensus.enterprises/">
    <img src="/images/consensus-logo-v1.1.svg">
  </a>
</div>

<br />
<br />
<br />
<br />
<br />
<br />


{{% /slide %}}

{{% slide id="hometitle" %}}

## Drupal SaaS
### Building software as a service on Drupal
#### [Colan Schwartz](https://consensus.enterprises/team/colan/)

<br />

##### [DrupalCamp Ottawa 2018](https://drupalcampottawa.com/#/schedule/f6659408-9238-4f67-817e-08d2820dba20)

<br />

<div class="whitebg">
  <a href="https://consensus.enterprises/">
    <img src="/images/consensus-logo-v1.1.svg">
  </a>
</div>


{{% /slide %}}
{{% /section %}}
