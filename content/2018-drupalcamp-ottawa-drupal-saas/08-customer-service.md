---
title: "Customer Service"
outputs: ["Reveal"]
weight: 80
---
{{% section id="customer-service" %}}
{{% slide id="customer-service" %}}

## Customer Service

{{% fragment %}}Public-facing issue tracker{{% /fragment %}}

<ul>
  {{% fragment %}}<li>[GitLab](https://about.gitlab.com/)'s [Service Desk](https://about.gitlab.com/features/service-desk/)</li>{{% /fragment %}}
  {{% fragment %}}<li>Associates customer tickets with project issues</li>{{% /fragment %}}
  {{% fragment %}}<li>Good option if already using GitLab<ul>
   {{% fragment %}}<li>And who isn't?</li>{{% /fragment %}}
  </ul></li>{{% /fragment %}}
  {{% fragment %}}<li>Other good options?</li>{{% /fragment %}}
</ul>

{{% /slide %}}

{{% /section %}}
