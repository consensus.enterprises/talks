---
title: "Goldilocks & the 3 Projects: Behat, BDD & Getting specs juuust Right"
outputs: ["Reveal"]
---
{{% section %}}

### Goldilocks & the 3 Projects: Behat, BDD & Getting specs juuust Right

##### [Dan Friedman](https://consensus.enterprises/team/dan/)

* **[DrupalTO Session](https://www.meetup.com/DrupalTO/events/274287188/)**
* **[YouTube Video](https://youtu.be/AYA-URqmw0g?t=946)**
* **[PDF Slides](Goldilocks-and-the-3-Projects-DrupalTO-Nov-2020.pdf)**
* **[Code Repository](https://gitlab.com/consensus.enterprises/talks/-/tree/master/content)**

<div class="whitebg">
  <a href="https://consensus.enterprises/">
    <img src="/images/consensus-logo-v1.1.svg">
  </a>
</div>

<br />
<br />
<br />
<br />
<br />
<br />

{{% /section %}}
