#############################################################################
# Provisioners
#
# @see https://github.com/hashicorp/terraform/issues/13418#issuecomment-292193469
#############################################################################

resource "null_resource" "provision_aegir" {
  depends_on = [
    openstack_compute_floatingip_associate_v2.public_ip_aegir0,
    openstack_compute_volume_attach_v2.va_1,
  ]

  connection {
    type = "ssh"
    user = "ubuntu"
    host = openstack_networking_floatingip_v2.floating_ip_aegir0.address
  }

  # Ensure the VM is up by connecting to it and running a command.
  provisioner "remote-exec" {
    inline = ["echo 'Confirming that the aegir VM is up & running.'"]
  }

  # Run ansible-galaxy to install ansible role dependencies.
  provisioner "local-exec" {
    command = "ansible-galaxy install -r requirements.yml -p roles"
    working_dir = "../ansible"
  }

  # Run the Ansible role to install Aegir.
  provisioner "local-exec" {
    command = "export ANSIBLE_STDOUT_CALLBACK=debug; export ANSIBLE_HOST_KEY_CHECKING=False; export ANSIBLE_FORCE_COLOR=True; ansible-playbook -vv --inventory ${openstack_networking_floatingip_v2.floating_ip_aegir0.address}, --extra-vars 'skip_host_key_validation=true' playbook.yml"
    working_dir = "../ansible"
  }
}
