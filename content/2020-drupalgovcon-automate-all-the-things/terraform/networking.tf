#############################################################################
# Networking
#############################################################################

resource "openstack_networking_network_v2" "main_network" {
  name           = "Main network"
  admin_state_up = "true"
}

resource "openstack_networking_subnet_v2" "main_subnet" {
  network_id = openstack_networking_network_v2.main_network.id
  cidr       = var.main_subnet_cidr
}

resource "openstack_networking_router_v2" "main_router" {
  name                = "Main router"
  external_network_id = var.external_network_id
}

resource "openstack_networking_router_interface_v2" "main_subnet_external_routing" {
  router_id = openstack_networking_router_v2.main_router.id
  subnet_id = openstack_networking_subnet_v2.main_subnet.id
}
