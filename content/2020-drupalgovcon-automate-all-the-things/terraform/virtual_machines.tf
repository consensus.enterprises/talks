#############################################################################
# Virtual machines (VMs)
#############################################################################

# Aegir / Web server VM #####################################################

resource "openstack_compute_instance_v2" "aegir" {
  count = 1
  name = "aegir${count.index}"
  image_id        = var.compute_image_id
  flavor_id       = var.compute_flavor_id_smallest
  key_pair        = "my-keypair"
  security_groups = ["public_ssh", "web"]
  network {
    name = "Main network"
  }
  depends_on = [
    openstack_networking_secgroup_v2.private_ssh,
    openstack_networking_secgroup_v2.web,
  ]
}

resource "openstack_blockstorage_volume_v2" "volume_1" {
  name = "volume_1"
  size = 20
}

resource "openstack_compute_volume_attach_v2" "va_1" {
  instance_id = openstack_compute_instance_v2.aegir[0].id
  volume_id   = openstack_blockstorage_volume_v2.volume_1.id
}

resource "openstack_networking_floatingip_v2" "floating_ip_aegir0" {
  pool = var.floatingip_pool
}
resource "openstack_compute_floatingip_associate_v2" "public_ip_aegir0" {
  floating_ip = openstack_networking_floatingip_v2.floating_ip_aegir0.address
  instance_id = openstack_compute_instance_v2.aegir[0].id
}
