#############################################################################
# Backend (state storage)
#############################################################################

terraform {
  backend "swift" {
    container         = "terraform-state"
    archive_container = "terraform-state-archive"
    # Use the NS data centre's object store (as that's the only one).
    auth_url = "https://keystone.ca-ns-1.clouda.ca:8443/v2.0"
    region_name = "regionOne"
  }
}

#############################################################################
# Cloud providers
#############################################################################

provider "openstack" {
  # Use the BC data centre's resources generally, as they're cheaper.
  auth_url = var.compute_auth_url
  region = var.compute_region_name
  # Authentication variables taken from environment after running your
  # OpenStack RC file, which authenticates a bash shell session to access the
  # APIs. Download it from https://dash.ca-bc-1.clouda.ca/project/api_access/.
}

#############################################################################
# SSH Keys
#############################################################################

resource "openstack_compute_keypair_v2" "my-keypair" {
  name       = "my-keypair"
  public_key = file(var.keypair_public_key)
}
