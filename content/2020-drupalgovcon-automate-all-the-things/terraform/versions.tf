terraform {
  required_providers {
    null = {
      source = "hashicorp/null"
      version = "~> 2.1.2"
    }
    openstack = {
      source = "terraform-providers/openstack"
      version = "~> 1.30.0"
    }
  }
  required_version = ">= 0.13"
}
