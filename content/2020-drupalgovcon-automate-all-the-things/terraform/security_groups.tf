#############################################################################
# Security groups
#############################################################################

# VPN servers ###############################################################
resource "openstack_networking_secgroup_v2" "vpn" {
  name        = "vpn"
  description = "VPN servers"
}

resource "openstack_networking_secgroup_rule_v2" "vpn_connections" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "udp"
  port_range_min    = var.vpn_port
  port_range_max    = var.vpn_port
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.vpn.id
}

# Private SSH servers #######################################################
resource "openstack_networking_secgroup_v2" "private_ssh" {
  name        = "private_ssh"
  description = "Private SSH servers"
}

resource "openstack_networking_secgroup_rule_v2" "private_ssh_connections" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_group_id   = openstack_networking_secgroup_v2.vpn.id
  security_group_id = openstack_networking_secgroup_v2.private_ssh.id
}

# Public SSH servers #######################################################
resource "openstack_networking_secgroup_v2" "public_ssh" {
  name        = "public_ssh"
  description = "Public SSH servers"
}

resource "openstack_networking_secgroup_rule_v2" "public_ssh_connections" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 22
  port_range_max    = 22
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.public_ssh.id
}

# Web servers ###############################################################
resource "openstack_networking_secgroup_v2" "web" {
  name        = "web"
  description = "Web servers"
}

resource "openstack_networking_secgroup_rule_v2" "http_connections" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 80
  port_range_max    = 80
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.web.id
}

resource "openstack_networking_secgroup_rule_v2" "https_connections" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 443
  port_range_max    = 443
  remote_ip_prefix  = "0.0.0.0/0"
  security_group_id = openstack_networking_secgroup_v2.web.id
}

# Database servers ##########################################################
resource "openstack_networking_secgroup_v2" "db" {
  name        = "db"
  description = "Database servers"
}

resource "openstack_networking_secgroup_rule_v2" "db_connections" {
  direction         = "ingress"
  ethertype         = "IPv4"
  protocol          = "tcp"
  port_range_min    = 3306
  port_range_max    = 3306
  remote_group_id   = openstack_networking_secgroup_v2.web.id
  security_group_id = openstack_networking_secgroup_v2.db.id
}

# TODO: Add egress rules for blocking outgoing traffic.
