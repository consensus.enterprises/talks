#############################################################################
# Automate-all-the-things Variables
#############################################################################

variable "compute_auth_url" {
  default = "https://keystone.ca-bc-1.clouda.ca:8443/v2.0/"
}
variable "compute_region_name" {
  default = "ca-bc-1"
}

variable "keypair_public_key" {
  default = "~/.ssh/id_rsa.pub"
}


variable "main_subnet_cidr" {
  default = "10.2.0.0/16"
}
variable "external_network_id" {
  # `openstack network list` -> "router_net"
  default = "46a9f7c1-7d22-48a8-878a-945afe2be462"
}

variable "compute_image_id" {
  # `openstack image list` -> Ubuntu 18.04 x86_64 (05/31/2018)
  default = "d0daa92c-d1cd-4bbb-a2b5-0c28fb4dfe85"
}
variable "compute_flavor_id_smallest" {
  # `openstack flavor list` -> 2 GB / 2 VCPU
  default = "69ffcf2e-5468-4e2c-95f3-81cf45545e70"
}

variable "floatingip_pool" {
  default = "ext_net"
}

variable "vpn_port" {
  default = "51820"
}
