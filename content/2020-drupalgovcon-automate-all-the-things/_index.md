---
title: "Automate all the things!"
outputs: ["Reveal"]
---
{{% section %}}
{{% slide id="automation-title" background-image="/images/automation/DCO-2018--16-9--Presentation-Slide.png" %}}

### Automate all the things!

##### [Christopher Gervais](https://consensus.enterprises/team/christopher/)

##### [Colan Schwartz](https://consensus.enterprises/team/colan/)

##### [Dan Friedman](https://consensus.enterprises/team/dan/)

* **[DrupalGovCon Session](https://www.drupalgovcon.org/2020/program/sessions/automate-all-things)**
* **[YouTube Video](https://www.youtube.com/watch?v=9W5lKK760rw&list=PLsGrHy_lmfhSV3zfr0HN4ZDnddHjcg2hq)**
* **[PDF Slides](Automate-all-the-things.DrupalGovCon.2020.pdf)**
* **[Code Repository](https://gitlab.com/consensus.enterprises/talks/-/tree/master/content)**

<div class="whitebg">
  <a href="https://consensus.enterprises/">
    <img src="/images/consensus-logo-v1.1.svg">
  </a>
</div>

<br />
<br />
<br />
<br />
<br />
<br />


{{% /slide %}}

{{% /section %}}
