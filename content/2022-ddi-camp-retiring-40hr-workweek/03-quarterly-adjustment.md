---
title: "Quarterly Adjustments"
outputs: ["Reveal"]
weight: 30
---
{{% section %}}
{{% slide id="salary-adjustments"  %}}

## Chapter 3:
### Quarterly salary adjustments

The big idea: calculate our paycheque based on how much we worked in the
previous 3 month period. Use our average weekly schedule over that period to
calculate how much we get paid for the current 3 months.

{{% /slide %}}
{{% slide id="salary-why" %}}

### Why?

* Accommodate workers who can't/won't do 40h/week (ie. full humans)
* Empower workers: we don't ask permission for time off, we set our own schedule

{{% /slide %}}
{{% slide id="salary-example" %}}

### Example

<small>
<ul style="margin-bottom: 1em;">
<li>$hourly == my hourly rate</li>
</ul>
<ul style="margin-bottom: 1em;">
{{% fragment %}}<li>I track hours for Q1 (Aug, Sep, Oct): I average 32hrs/week.</li>{{% /fragment %}}
{{% fragment %}}<li>Start of Q2: Payroll adjusted so I get paid $hourly * 32hrs/week Nov-Jan</li>{{% /fragment %}}
</ul>
<ul style="margin-bottom: 1em;">
{{% fragment %}}<li>I track hours for Q2 (Nov, Dec, Jan): I average 35hrs/week</li>{{% /fragment %}}
{{% fragment %}}<li>Start of Q3: Payroll adjusted so I get paid $hourly * 35hrs/week Feb-Apr</li>{{% /fragment %}}
</ul>
<br />
<ul>
{{% fragment %}}<li>I track hours for Q3 (Feb, Mar, Apr): I average 30hrs/week</li>{{% /fragment %}}
{{% fragment %}}<li>Start of Q4: Payroll adjusted so I get paid $hourly * 30hrs/week May-Jul</li>{{% /fragment %}}
</ul>
</small>

{{% /slide %}}
{{% slide id="salary-pros" %}}

### Pros

- Employee satisfaction (agency)
- Small changes in our schedule average out
- Less administrative overhead (time off etc)
- Unpaid overtime is not a thing: we just get paid for our work
- No need to explain "life happens" situations

{{% /slide %}}
{{% slide id="salary-cons" %}}

### Cons?

<ul>
{{% fragment %}}<li>If you work a consistent schedule, nothing changes.</li>{{% /fragment %}}
{{% fragment %}}<li>We're only just extending this to all employees (but everyone <em>wants</em> it!)</li>{{% /fragment %}}
{{% fragment %}}<li>We had to figure out some legal aspects to incorporate this into employment contracts</li>{{% /fragment %}}
</ul>

{{% note %}}

We tried to identify CONs, and genuinely couldn't think of any we've experienced.

{{% /note %}}
{{% /slide %}}
{{% slide id="salary-challenges" %}}

### Challenges

* Bringing new recruits into the process is somewhat tricky.
* Leaving the company is similarly tricky.

{{% note %}}

* Onboarding: Our solution is to **pay hourly during probation period**, to allow for establishing at least one quarter of data.

* Leaving: We pay out the difference if your schedule was higher in the current quarter than the one previous to your exit.

* Leaving: We write off the difference if your schedule was lower in the final quarter you worked.

{{% /note %}}
{{% /slide %}}
{{% slide id="salary-more-challenges" %}}

### Moar Challenge

<ul>
{{% fragment %}}<li>Our model presupposes a trust in our workforce to achieve results on their own schedule.</li>{{% /fragment %}}
{{% fragment %}}<li>This does not solve the problem of people faking their timesheets.</li>{{% /fragment %}}
{{% fragment %}}<li>Transparency and granularity in our time-tracking allows for a certain amount of accountability.</li>{{% /fragment %}}
{{% fragment %}}<li>Will this scale? We think so.</li>{{% /fragment %}}
</ul>

{{% note %}}

Frankly, we don't want to work with people we don't trust.

We have found that extending our trust, combined with transparency and respect, our workers return it in kind.

Paying attention to the results we produce over the time we sit in a chair is HUGE.

{{% /note %}}
{{% /slide %}}
{{% /section %}}
