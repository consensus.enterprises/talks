---
title: "Questions"
outputs: ["Reveal"]
weight: 90
---
{{% section %}}
{{% slide id="questions" %}}

## Questions?


{{% note %}}

What unexpected challenges surfaced, or unforeseen advantages?

what research went into the decision?
  * we had to talk to employment lawyers to ensure this was legal, and to draft
    contract language

What challenges were there in adoption?
  * bootstrapping into it is tricky
  * also, exiting the process is tricky

Why do this based on time-tracking, rather than output?
  * because we're billing clients hourly

{{% /note %}}
{{% /slide %}}
{{% /section %}}
