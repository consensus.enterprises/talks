---
title: "Growing"
outputs: ["Reveal"]
weight: 80
---
{{% section %}}
{{% slide id="growing-questions" %}}

### Questions arising from growth...

* How do we:
  * equitably account for the impact we have on the success of the company?
  * identify and adjust the behaviours we value as an organization?

{{% /slide %}}
{{% slide id="growing-questions2" %}}

### More questions...

* How do we:
  * moderate our intrinsic biases while seeking objective assessment metrics?
  * work against pay gaps based on gender, race, etc. and create radical transparency in our salary process?
  * recognize that plenty more than just engineering skill goes into a successful technology company?

{{% /slide %}}
{{% slide id="growing-questions3" %}}

### And even more questions...

* How do we:
  * improve the agency we have as workers to improve our salary?
  * minimize the impact of negotiation and other skills that are unevenly distributed along lines of oppression?

{{% /slide %}}
{{% slide id="growing-answer" %}}

### Our answer: competency-driven peer-reviewed metrics

1. Recognize **diversity is our strength**, and double down on it. Seek it out in as many aspects as possible.
2. Understand **competencies** as key behaviours that impact the success of the organization.
3. Our tool: **competency-driven, equity-informed peer-reviewed metrics** for how we're doing and how to improve.

{{% /slide %}}
{{% /section %}}
