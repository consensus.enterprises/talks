---
title: "Sociocracy"
outputs: ["Reveal"]
weight: 70
---
{{% section %}}
{{% slide id="sociocracy-slide" %}}

## Sociocracy

* Scaling up consensus decision making
* Governance model that distributes power, empowers and invites diverse voices
* https://www.sociocracyforall.org/start-here/

{{% /slide %}}
{{% /section %}}
