---
title: "Resources"
outputs: ["Reveal"]
weight: 50
---
{{% section %}}
{{% slide id="resources" %}}

## Resources

0. Context
1. The policy
2. Employment contract clause
3. The tool

{{% /slide %}}
{{% slide id="resources-context" %}}

### Context

<ul>
{{% fragment %}}<li>Harvest is our time tracker (but any similar tool would do)</li>{{% /fragment %}}
{{% fragment %}}<li>This is easy for us because we track time for clients already</li>{{% /fragment %}}
</ul>

{{% /slide %}}

{{% slide id="resources-policy" %}}

### Policy goals

<blockquote style="width: 100%; text-align: left;"><small>

This policy describes why and how we adjust our payroll on a quarterly basis to determine our monthly salaries for the next quarter.  Our policies and process for adjusting salary factors *must* align with our **values**.

#### Goals

The goals of this policy are to:
* Allow for self-management around work schedules
* Remove any need to get approval for sick days, vacation schedules, etc.
* Provide transparency in how we get paid:
    * This makes our salaries auditable
    * It allows us to reason effectively about our individual salaries, as well as our overall payroll
* Stabilize salaries by flattening out variations in our schedule over the entire quarter.

</small></blockquote>

{{% /slide %}}
{{% slide id="resources-policy2" %}}

### The policy

<blockquote style="width: 100%; text-align: left;"><small>

#### Policy

At the beginning of each quarter we collect time-tracking data for all staff for the previous quarter. We then calculate payroll for the new quarter based on efforts over the previous quarter.

We incorporate holidays, vacation and sick days, by removing them from the denominator when calculating average hours per week/month.

We use each staff member's *individual base hourly rate* to calculate their paycheque amount for the quarter.

</small></blockquote>

{{% /slide %}}

{{% slide id="resources-ianal" %}}

## IANAL

<ul>
{{% fragment %}}<h3>Please note: I, Derek</h3>{{% /fragment %}}
{{% fragment %}}<h5>and I cannot stress this enough</h5>{{% /fragment %}}
{{% fragment %}}<h2>am not a lawyer</h2>{{% /fragment %}}
</ul>

{{% /slide %}}

{{% slide id="resources-contracts" %}}

### Employment contract clause

<blockquote style="width: 100%; text-align: left;"><small>
Our compensation model is based on promoting financial stability while ensuring maximum
flexibility with respect to hours of work.<br />
To that end, salaries are adjusted on a quarterly basis based on hours worked
the previous quarter. This means that, if your hours in one quarter significantly exceed those the
following quarter, you may see a decrease in salary; conversely, if your hours
in one quarter are lower than those the subsequent quarter, you will see an
increase in salary.<br />
The particulars of our calculations, and the considerations applied to compensation
are set out in the salary calculation procedure available upon request.<br />
By signing this Agreement, you agree and acknowledge that your salary may
fluctuate from one quarter to the next and agree and understand that any
reduction in salary shall not be construed as a unilateral change to your
employment agreement and does not constitute constructive dismissal.<br />
</small></blockquote>

{{% note %}}
I AM NOT A LAWYER
{{% /note %}}
{{% /slide %}}
{{% slide id="resources-tool" %}}

### The Tool

Good news: we're releasing our spreadsheet, instructions, and other resources as open source software (GPLv3):

* [https://gitlab.com/consensus.enterprises/flexible-salary-tools/](https://gitlab.com/consensus.enterprises/flexible-salary-tools/)
  * [consensus-flextime.ods](https://gitlab.com/consensus.enterprises/flexible-salary-tools/-/blob/main/consensus-flextime.ods)
  * [employment-contract-clause.md](https://gitlab.com/consensus.enterprises/flexible-salary-tools/-/blob/main/employment-contract-clause.md)
  * [quarterly-adjustment-policy.md](https://gitlab.com/consensus.enterprises/flexible-salary-tools/-/blob/main/quarterly-adjustment-policy.md)
  * [quarterly-adjustment-instructions.md](https://gitlab.com/consensus.enterprises/flexible-salary-tools/-/blob/main/quarterly-adjustment-policy.md)

{{% /slide %}}
{{% /section %}}
