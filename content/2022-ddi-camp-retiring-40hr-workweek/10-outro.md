---
title: "Outro"
outputs: ["Reveal"]
weight: 100
---
{{% section %}}
{{% slide id="outro" %}}

## Thank You!

<ul style="width: 100%">
<li> <a href="https://bit.ly/consensus-flextime">https://bit.ly/consensus-flextime</a>
<li> <a href="https://talks.consensus.enterprises/2022-ddi-camp-retiring-40hr-workweek">talks.consensus.enterprises/2022-ddi-camp-retiring-40hr-workweek</a></li>
<li> <a href="https://gitlab.com/consensus.enterprises/flexible-salary-tools">gitlab.com/consensus.enterprises/flexible-salary-tools</a></li>
<li> <a href="https://consensus.enterprises/blog/salary-calculator">consensus.enterprises/blog/salary-calculator</a></li>
</ul>

{{% note %}}

# Key Takeaways:

- Results-oriented workplaces empower us to collaborate and do our best work
- You can incorporate some of these ideas into your organization

{{% /note %}}
{{% /slide %}}
{{% /section %}}
