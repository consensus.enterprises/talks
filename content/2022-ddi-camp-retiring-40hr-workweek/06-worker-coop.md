---
title: "Worker Co-op"
outputs: ["Reveal"]
weight: 60
---
{{% section %}}
{{% slide id="theres-more" %}}

# But wait.. there's more!

{{% note %}}

The "consensus flextime" process is just one way we're institutionalizing
fairness across the organization.

I want to give a quick teaser of some of the other initiatives we're pursuing
along similar lines.

{{% /note %}}
{{% /slide %}}
{{% slide id="worker-coop" %}}

## Worker-owned
## Co-operative

* Workers own the capital
* Democratic control (one member one vote)
* https://en.wikipedia.org/wiki/Worker_cooperative

{{% note %}}

A great example of institutionalizing fairness: ensure the benevolence of the
founding partners is not the only reason the company runs the way it does.

{{% /note %}}
{{% /slide %}}
{{% /section %}}
