---
title: "Demo"
outputs: ["Reveal"]
weight: 40
logo: true
---
{{% section %}}
{{% slide id="tour-how" %}}

## Tour: How do we do it?

{{% /slide %}}
{{% slide id="tour-how1" %}}

#### 1. At the start of each quarter, pull 3 monthly reports from your time tracking tool for each member of the team.

{{% note %}}

Plug numbers for each month of the quarter into the tool.

{{% /note %}}

{{% /slide %}}
{{% slide id="tour-how2"%}}

#### 2. The tool knows how many work days were in the quarter, accounting for holidays & weekends.

{{% /slide %}}
{{% slide id="tour-how3" %}}

#### 3. Incorporate any time off during the quarter, and calculate an average hours / week.

{{% /slide %}}
{{% slide id="tour-how4" %}}

#### 4. Multiply that by hourly rate to get weekly pay.

{{% /slide %}}
{{% slide id="tour-config" %}}

## Tour: Config

{{% note %}}
1. Config sheet
  - start of fiscal year
  - table of Employees
  - table of Holidays

* Note this changes very rarely

{{% /note %}}

{{% /slide %}}
{{% slide id="demo2" %}}

## Tour: Calculations

{{% note %}}

2. Calculations sheet
  - this sheet is for intermediate calculations and lookups
  - Quarter start/end dates
  - Time off by quarter (more detail in a moment)
  - Net workdays per quarter
  - Holidays per province
  - Holidays per quarter
  - employee offset (a dark corner)

* These change almost never.

{{% /note %}}
{{% /slide %}}
{{% slide id="tour-timeoff" %}}

## Tour: Time off

{{% note %}}

3. Time off sheet
  - Set of columns per employee
  - Dates down column A, with B and C showing:
    - whether that day is a weekday, and
    - which quarter it's in
  - Explain V, S, and O
  - Note tally rows per employee in the top 5 rows

* This changes as people take time off

{{% /note %}}
{{% /slide %}}
{{% slide id="tour-payroll" %}}

## Tour: Payroll - Q1

{{% note %}}

4. Payroll - Q1
  - Yellow-background cells are for data entry
    - 3 monthly cells pulled from a Harvest report
    - hourly rate can vary by employee
  - Days off in Q1 is carried over from the previous fiscal year's data.
    - in Q2 this will pull from the Time Off sheet in this year's document.
  - `Days per Quarter` calculated based on Net workdays per quarter MINUS days off
  - `Avg hours per week` is calculated as: `Hrs per quarter / Days per Quarter` to get a daily average hours worked
    - then multiplied by 5 days in the work week
    - then rounded to the nearest 15 mins
  - Payroll values are then calculated by multiplying the `Avg hours per week` by the hourly rate.
    - these become the numbers that we pay people for all of Q2

* This happens quarterly

{{% /note %}}
{{% /slide %}}
{{% slide id="tour-payroll-q2" %}}

## Tour: Payroll - Q2

{{% note %}}

5. Payroll - Q2
   - note that Days off values are pulled from Time Off sheet now
   - plug in some numbers for the 3 employees
   - show the values get calculated.

{{% /note %}}
{{% /slide %}}
{{% /section %}}
