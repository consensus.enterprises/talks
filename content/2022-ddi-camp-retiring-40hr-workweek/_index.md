---
title: "Retiring the traditional 40 hour work week: Designing a compensation model for the 21st century."
outputs: ["Reveal"]
logo: true
---
{{% section %}}
{{% slide id="hometitle" %}}

## Retiring the traditional 40 hour work week:
### Designing a compensation model for the 21st century.
#### Derek Laventure
##### derek@consensus.enterprises

<small>

* [Video recording](https://www.youtube.com/watch?v=6j_5n1AUolI)
* [Slides](https://talks.consensus.enterprises/2022-ddi-camp-retiring-40h-workweek)
* [Drupal Diversity Camp 2022](https://www.drupaldiversity.com/initiatives/ddi-camp-2022/program)

</small>

{{% note %}}

Consensus is trying some unique approaches to worker compensation, and we think you might learn something from it.

Please feel free to follow along

{{% /note %}}
{{% /slide %}}
{{% /section %}}
