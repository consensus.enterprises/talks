---
title: "Introduction"
outputs: ["Reveal"]
weight: 10
showlogo: True
---
{{% section %}}
{{% slide id="chapter-0" %}}

## Chapter 0: What are we talking about?

{{% note %}}
Key takeaways:
- Results-oriented workplaces empower us to collaborate and do our best work
- You can incorporate some of these ideas into your organization

- We value our humanity first
- We have a salary process that gives us a lot of control over our work schedule.

- I'll tell you the story of how we got here
- Then give a TOUR of the tool itself

- This process has worked well for us for several years, and we are excited to share it!
- We've opensourced our tool and some related documentation as a reference.
{{% /note %}}
{{% /slide %}}
{{% slide id="intro" %}}

## Chapter 1: In the beginning...

<div style="float: left;">

![Consensus Founders](images/cei-founders.png)

</div>

<p><br />4 senior technology professionals working together.</p>
<p>Our primary goal: make a living to support our families.</p>
<p>Beyond that: we had.. ambitions ;)</p>

{{% /slide %}}
{{% slide id="intro-now" %}}

### 4 Years on...

* [Matt Parker](https://consensus.enterprises/team/matt)
* [Serena Zhu](https://consensus.enterprises/team/serena)
* [Scott Reeves](https://consensus.enterprises/team/scott) <a style="text-align: left;" href="https://github.com/drupaldiversity/administration/issues/17"><img width="15px" height="15px" src="images/ddi-favicon.ico" alt="DDI Logo linking to issue where Scott created the DDI logo" title="Did you know Scott designed the DDI logo?" /></a>
* [Brian Sharpe](https://www.linkedin.com/in/brian-s-49762523b/)
* [Matei Stanca](https://ambientimpact.com)
* [Seonaid Lee](https://www.linkedin.com/in/seonaid-lee/) (honourable mention)

{{% note %}}

To QUICKLY give you an idea of where we are now, and where we want to go:

- we've grown our team to 9!
- Seonaid has gone off a side-quest to work on ethics in tech. GO CHECK HER OUT!

- we've developed and articulated guiding texts:
  - a shared worldview
  - core values
  - mission & vision statements

{{% /note %}}
{{% /slide %}}
{{% slide id="intro-worldview" %}}

### Shared Worldview

* **Truth matters.**
* **People come first.**
* **We are mission- and values-driven.**
* **Nothing is written in stone.**
* **A group is more than the sum of its parts.**
* **Information wants to be free** but **privacy must be respected.**

{{% note %}}

* **TRUTH** Data and good faith debate with diverse voices are how we build a shared understanding of reality.

* **PEOPLE** We must prioritize people over profit. No one should have to compromise their values for material gain.

* **VALUES** Our policies must be driven by our goals.

* **STONE** We must evolve, grow and change.

* **GROUP** Through distributed and flexible leadership we strive for healthy dynamics to
  unlock the power of the group

* **FREEDOM** Copyleft software, documentation and culture is the only way to ensure private information remains private.

{{% /note %}}
{{% /slide %}}
{{% slide id="intro-values" %}}

### Consensus Values:

<ol>
{{% fragment %}}<li>Trust and respect</li>{{% /fragment %}}
{{% fragment %}}<li>Everyone's voice</li>{{% /fragment %}}
{{% fragment %}}<li>Professional maturity</li>{{% /fragment %}}
{{% fragment %}}<li>Positive impact</li>{{% /fragment %}}
{{% fragment %}}<li>Solidarity</li>{{% /fragment %}}

{{% note %}}

1. Mutual **trust** and **respect** form the foundation of any strong organization
2. Everyone's voice matters, so we should seek *consensus* on important decisions; recognizing and respecting that we are *equal but different.*
3. Professional maturity is fundamental to our success, so we must encourage a **growth mindset**
4. We all strive to **make a positive impact** so we must support each other in seeking out new **opportunities** and **challenges**.
5. Solidarity with each other and our communities; contributing, collaborating, supporting and sharing to promote our mutual success.

{{% /note %}}
{{% /slide %}}
{{% slide id="intro-circles" %}}

### Sociocracy

<img src="images/cei-circles.png" style="width: 100%" alt="Consensus Circle Structure" />

{{% note %}}

We've grown "departments" in the form of sociocratic circles, which I'll talk about in some more detail later

This is our "org" structure and governance model

{{% /note %}}
{{% /slide %}}
{{% /section %}}
