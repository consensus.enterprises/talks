---
title: "Social Location"
outputs: ["Reveal"]
weight: 1
---
{{% section %}}
{{% slide id="land-back" %}}

## Land Back!

I am in Kjipuktuk (Halifax) in Mi'kma'ki, ancestral and unceded territory of the Mi'kmaq People.

I offer respect and gratitude to the people who are past, present and future caretakers of this land.

Where are you?  https://native-land.ca/

{{% note %}}

I want to acknowledge that I live and work...

Deep gratitude and respect...

I encourage you to take a moment to

think about the territory and peoples who are indigenous to wherever you are..

{{% /note %}}
{{% /slide %}}
{{% slide id="social-location" %}}

## Who am I?

### Derek Laventure (he/they)

<div>
  <img style="float: left; width: 20%; margin-right: 0.5em;" src="images/derek-headshot.jpg" alt="Portrait of Derek, white face with broad smile and glasses." />

  <ul style="float: right; width: 75%; margin-left: 0.5em;">
  <li>White middle-class able-bodied (gender)queer settler</li>
  <li>BSc Computer Science<br />(University of Toronto 2000)</li>
  <li>20+ years of Drupal (<a href="https://drupal.org/u/spiderman">spiderman@drupal.org</a>)</li>
  <li>Founding partner of Consensus</li>
  <li>Tai Chi student and instructor</li>
  </ul>
</div>

{{% note %}}

Just to further situation myself, my name is Derek Laventure

I generally use he and they pronouns, and these are some of the intersectional
labels that apply to me :)

{{% /note %}}
{{% /slide %}}

{{% slide id="acknowledgements" %}}

## Gratitude

I want to thank...

<ul>
{{% fragment %}}<li>Fei Lauren</li>{{% /fragment %}}
{{% fragment %}}<li>DDI Camp organizers</li> {{% /fragment %}}
{{% fragment %}}<li>My incredible <a href="https://consensus.enterprises/team">team at Consensus</a></li>{{% /fragment %}}
{{% fragment %}}<li>YOU</li>{{% fragment %}}
</ul>

{{% note %}}
- I AM EXCITED TO BE HERE

- **FEI** for suggesting I submit this talk, and for their support and encouragement as I put it together.
- **DDI Camp** for selecting my talk, and putting together an AMAZING event.
- **Consensus Team** for relentlessly positive support, feedback, and ideation.
- **YOU** the curious and interested folks showing up to hear me talk :)
{{% /note %}}
{{% /slide %}}
{{% /section %}}
