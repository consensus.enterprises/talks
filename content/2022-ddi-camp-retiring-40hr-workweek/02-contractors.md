---
title: "Contractors to Employees"
outputs: ["Reveal"]
weight: 20
logo: true
---
{{% section %}}
{{% slide id="contractors" %}}

## Chapter 2: Becoming Employees

{{% note %}}

* As contractors, we invoiced Consensus monthly

{{% /note %}}
{{% /slide %}}
{{% slide id="contractors-salary"  %}}

### Going on Salary

{{% note %}}

* Once the org achieved some stability, we wanted to transfer that to ourselves: salary

{{% /note %}}
{{% /slide %}}
{{% slide id="contractors-flexibility"  %}}

### Retaining flexibility

{{% note %}}

* BUT: we wanted to keep the flexibility of setting our own schedule.

{{% /note %}}

{{% /slide %}}
{{% /section %}}
