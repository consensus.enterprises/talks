# Consensus Enterprises: Talks & Presentations

## To get started

1. `git clone --recursive git@gitlab.com:consensus.enterprises/talks.git`
1. `cd talks`
1. `hugo serve`

## Documentation

* [Reveal Hugo | Hugo Themes](https://themes.gohugo.io/reveal-hugo/)
* [reveal.js - The HTML Presentation Framework](https://revealjs.com/)
